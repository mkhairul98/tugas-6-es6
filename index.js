
  // jawaban soal 1
  function luasPersegi(panjang, lebar) {
    return panjang * lebar;
  }
  
  function kelilingPersegi(panjang, lebar) {
    return 2 * (panjang + lebar);
  }
  
  function hitungPersegi(panjang, lebar) {
    const luas = luasPersegi(panjang, lebar);
    const keliling = kelilingPersegi(panjang, lebar);
    return [luas, keliling];
  }
  
  const [luas, keliling] = hitungPersegi(5, 4);
  
  console.log('luas persegi adalah', luas);
  console.log('keliling persegi adalah', keliling);

  //jawaban soal 2


  const nama = (namaDepan, namaBelakang) => {
    let namaLengkap = {
      firstName: namaDepan,
      lastName: namaBelakang,
    };
    
    console.log(`${namaLengkap.firstName} ${namaLengkap.lastName}`);
  };
  nama("Muhammad", "khairul");

//jawaban soal 3

const data = {
    firstName: "Muhammad",
    lastName: "khairul",
    address: "Jalan parompong",
    hobby: "playing football",
  }
  

const {firstName,lastName,address,hobby} = data;
console.log(firstName,lastName,address,hobby)


//jawaban soal 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["west","Gill", "Brian", "Noel", "Maggie"]
//const combined = [...west, ...east]
console.log (east)


//jawaban soal 5

const planet = "earth"
const view = "glass"
console.log(`Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`)
